# FEmto's a Makefile TOol #

**Femto** is a little Makefile tool designed to offer simple configuration
capabilities for the programmers tired of complicated build system like
autotools. It is consists only on 100 lines of Makefile code that can
easily be added to your existing Makefile.

A simple `make config` command allows generating a configuration that
can easily be accessed by Makefiles and code via `config.h` and `femto-config.mk`.

## Get Started ##

**Femto** is  designed to be used with `gmake` it is not compatible with
4.2bsd `make`. Getting started is really simple: just copy the file `femto.mk`
to your project directory and add the following line to your `Makefile`:

	include femto.mk

This inclusion define **femto** configurations targets, so now you can already
use `make config` to perform the configuration of the project. Of course,
without further modifications, no tests will be performed, so this will be quite
useless. You have to use set **femto** variables to specify tests and options
that must be performed.

- `PROGRAMS = prog1 prog2 ...` : Asks **femto** to check for programs `prog1` and so on.
The results of the test can then be accessed using two variables : `prg_prog1`
and `path_prog1`. The first indicates whether the program has been found (if set to 1),
the second indicates the path of it.
- `FUNCTIONS = fn1 fn2 ...` : Asks **femto** to check for functions `fn1` and so on.
The results of the test can then be accessed using `fn_fn1` which is set to 1 if
`fn1` exists. Note that this uses a template `config.c.in` to generate the test file,
in which every occurence of `@fn@` will be replaced by the function tested. A modified
version of function name is available for preprocessor directives through `@fnp@`.
- `LIBS = lib1 lib2 ...` : Asks **femto** to check for lib `lib1` and so on.
The results of the test can then be accessed using `lib_lib` which is set to 1 if
`lib1` exists.
- `OPTIONS = option1 option2 ...` : A list of options that can be switched on or
off using `use-option1` or `no-option1` target and so on. The configuration can be accessed
using `use_option1` variable, which is equal to 1 if `option1` is set.
- `DEFAULTOPTIONS = use-option1 no-option2 ...` : A list describing the default
state of options defined by `OPTIONS`.
- `SUBCONFIG = subdir1 subdir2 ...` : A list of sub-directories using **femto** that
must be configured.
- `ADDITIONALVARS = var1 var2 ...` : A list of variables that must be made
accessible via `config.h.in`

## Accessing configuration variable ##

### From C code ###

Configuration variables can be accessed from the C code using the file `config.h`
generated from `config.h.in`.

`config.h.in` must be a template where every `@variable@` will be replaced
by the corresponding configuration `variable`.

### From Makefile ###

Configuration variables can be accessed from Makefiles by including `femto-config.mk` :

	ifneq (,$(wildcard ./femto-config.mk))
		include femto-config.mk
	endif
	
## From the shell ##

Configuration variables can be accessed through the shell using the automatically
generated script `femto-subst`. It expands every femto `@variable@` from its input
and output the result.
	
## Examples ##

An example of **femto** use can be found in the [dos9](http://dos9.org) source code,
in the root and `libcu8` folder.

## License ##

**femto** is distributed under the terms of the MIT license.


